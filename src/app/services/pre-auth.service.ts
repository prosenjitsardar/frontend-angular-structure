import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  SignupReq,
  ResendOtpReq,
  ForgotPasswordReq,
  ResetPasswordReq,
  LoginWithOtpReq,
  LoginReq,
  AuthInfo,
  ChangePasswordReq
} from '../types/interface.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config } from '../config';
import { LocalStorageService } from 'src/app/services/utilities/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class PreAuthService {

  private BASE_API_URL = Config.BASE_API_URL;

  userEmail = new BehaviorSubject<string>('');
  userEmailOrPhone = new BehaviorSubject<string>('');

  constructor(
    private http: HttpClient,
    private _localStorageService: LocalStorageService,
    private router: Router
  ) { }

  signup(userDetails: SignupReq): Observable<any> {
    return this.http.post<SignupReq>(`${this.BASE_API_URL}user/signup`, userDetails)
  }

  resendOtp(userDetails: ResendOtpReq): Observable<any> {
    return this.http.post<ResendOtpReq>(`${this.BASE_API_URL}user/resend-otp`, userDetails)
  }

  forgotPassword(userDetails: ForgotPasswordReq): Observable<any> {
    return this.http.post<ForgotPasswordReq>(`${this.BASE_API_URL}user/forgot-password`, userDetails)
  }

  resetPassword(userDetails: ResetPasswordReq): Observable<any> {
    return this.http.post<ResetPasswordReq>(`${this.BASE_API_URL}user/reset-password`, userDetails)
  }

  changePassword(userDetails: ChangePasswordReq): Observable<any> {
    return this.http.post<ChangePasswordReq>(`${this.BASE_API_URL}user/change-password`, userDetails)
  }

  loginWithOtp(userDetails: LoginWithOtpReq): Observable<any> {
    return this.http.post<LoginWithOtpReq>(`${this.BASE_API_URL}user/login-with-otp`, userDetails)
  }

  login(userDetails: LoginReq): Observable<any> {
    return this.http.post<LoginReq>(`${this.BASE_API_URL}user/login`, userDetails)
  }

  logout() {
    this.removeAuthInfo();
    this.router.navigate(['/login']);
  }

  refreshToken(): Observable<any> {
    return this.http.get<any>(`${this.BASE_API_URL}user/token`)
  }

  isLoggedIn() {
    return !!this._localStorageService.getAccessToken();
  }

  storeAuthInfo(authInfo: AuthInfo) {
    this._localStorageService.storeAuthInfo(authInfo);
  }

  getAuthInfo() {
    return this._localStorageService.getAuthInfo();
  }

  updateAuthInfo(authInfo: AuthInfo) {
    this._localStorageService.updateAuthInfo(authInfo)
  }

  removeAuthInfo() {
    this._localStorageService.removeAuthInfo();
  }

}
