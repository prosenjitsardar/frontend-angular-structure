import { Injectable } from '@angular/core';

import { CryptoService } from 'src/app/services/utilities/crypto.service';
import { AuthInfo } from 'src/app/types/interface.model';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private readonly AUTH_INFO = 'AUTH_INFO';

  constructor(private _cryptoService: CryptoService) { }

  storeAuthInfo(authInfo: AuthInfo) {
    /* Converting authInfo JSON object into string object. Then encrypting and storing that object*/
    const stringAuthInfo = JSON.stringify(authInfo);
    const encryptedAuthInfo = this._cryptoService.encrypt(stringAuthInfo);
    localStorage.setItem(this.AUTH_INFO, encryptedAuthInfo);
  }

  getAuthInfo() {
    if (localStorage.getItem(this.AUTH_INFO) == null) {
      return null;
    } else {
      const decryptedAuthInfo = this._cryptoService.decrypt(localStorage.getItem(this.AUTH_INFO));
      const authInfo: AuthInfo = JSON.parse(decryptedAuthInfo);
      return authInfo;
    }
  }

  updateAuthInfo(authInfo: AuthInfo) {
    localStorage.removeItem(this.AUTH_INFO);
    const stringedAuthInfo = JSON.stringify(authInfo);
    const encryptedAuthInfo = this._cryptoService.encrypt(stringedAuthInfo);
    localStorage.setItem(this.AUTH_INFO, encryptedAuthInfo);
  }

  removeAuthInfo() {
    localStorage.removeItem(this.AUTH_INFO);
  }

  getAccessToken() {
    if (localStorage.getItem(this.AUTH_INFO) == null) {
      return null;
    } else {
      const decryptedAuthInfo = this._cryptoService.decrypt(localStorage.getItem(this.AUTH_INFO));
      const authInfo: AuthInfo = JSON.parse(decryptedAuthInfo);
      return authInfo.accessToken;
    }
  }

  getRefreshToken() {
    if (localStorage.getItem(this.AUTH_INFO) == null) {
      return null;
    } else {
      const decryptedAuthInfo = this._cryptoService.decrypt(localStorage.getItem(this.AUTH_INFO));
      const authInfo: AuthInfo = JSON.parse(decryptedAuthInfo);
      return authInfo.refreshToken;
    }
  }

}
