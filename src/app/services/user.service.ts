import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config } from '../config';
import { IUser } from '../types/interface.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userDetails = new BehaviorSubject<IUser>(null);
  private BASE_API_URL = Config.BASE_API_URL;

  constructor(private http: HttpClient,) { }

  getUserDetails(): Observable<any> {
    return this.http.get<any>(`${this.BASE_API_URL}user/me`)
  }

  uploadProfileImage(formdata: any): Observable<any> {
    return this.http.post<any>(`${this.BASE_API_URL}user/profile-image`, formdata)
  }
}
