export interface SignupReq {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    password: string;
}

export interface ResendOtpReq {
    emailOrPhone: string;
}

export interface ForgotPasswordReq {
    emailOrPhone: string;
}

export interface ResetPasswordReq {
    emailOrPhone: string;
    newPassword: string;
    otp: string;
}

export interface LoginWithOtpReq {
    email: string;
    otp: string;
}

export interface LoginReq {
    emailOrPhone: string;
    password: string;
}

export class AuthInfo {
    accessToken: string;
    accessTokenExpiry: number;
    refreshToken: string;
    refreshTokenExpiry: number;
    name: {
        first: string,
        last: string
    };
    email: string;
    phone: string;
}

export interface ChangePasswordReq {
    password: string;
    newPassword: string;
}

export interface IUser {
    _id: string;
    name: {
        first: string,
        last: string
    };
    email: string;
    phone: string;
    profileImage: string;
}