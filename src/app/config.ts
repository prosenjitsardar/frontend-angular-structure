import { environment } from '../environments/environment';

export const Config = {
    SERVER_HOST: 'http://localhost:4000/',
    BASE_API_URL: environment.production ? environment.BASE_API_URL : 'http://localhost:4000/api/v1/',
    X_API_KEY: environment.production ? environment.X_API_KEY : '8qz5gaEXqqH98&Q5USDNHm$uHUq*xADs&Z*9Hxfq$6vE!&$p#9',
    CRYPTO_KEY: environment.production ? environment.CRYPTO_KEY : 'YourSecretKeyForEncryption&Descryption'
}