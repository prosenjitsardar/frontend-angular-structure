import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { AuthInfo } from '../types/interface.model';
import { catchError, switchMap, filter, take } from 'rxjs/operators';
import { PreAuthService } from 'src/app/services/pre-auth.service';
import { Config } from '../config';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private BASE_API_URL = Config.BASE_API_URL;

  constructor(private _preAuthService: PreAuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authInfo: AuthInfo = this._preAuthService.getAuthInfo();
    if (authInfo) {
      // set diffrent token for refresh token request
      request.url === `${this.BASE_API_URL}user/token`
        ? (request = this.addToken(request, authInfo.refreshToken))
        : (request = this.addToken(request, authInfo.accessToken));
    }

    return next.handle(request).pipe(
      catchError((error) => {
        error.status = error['error code'];
        if (error.status === 401) {
          return this.handle401Error(request, next);
        } else {
          return throwError(error);
        }
      })
    );
  }

  private addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        authorization: `Bearer ${token}`,
      },
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      return this._preAuthService.refreshToken()
        .pipe(
          switchMap((res: any) => {
            const { accessToken } = res;
            this._preAuthService.updateAuthInfo(res);
            this.isRefreshing = false;
            this.refreshTokenSubject.next(accessToken);
            return next.handle(this.addToken(request, accessToken));
          })
        )
        .pipe(
          catchError((error) => {
            /**
             * handle refresh token error
             * just removing token from storage for customer
             * Redirection will required for SP
             */
            this._preAuthService.logout();
            return throwError(error);
          })
        );
    } else {
      return this.refreshTokenSubject.pipe(
        filter((token) => token != null),
        take(1),
        switchMap((jwt) => {
          return next.handle(this.addToken(request, jwt));
        })
      );
    }
  }
}