import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PreAuthService } from 'src/app/services/pre-auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ResendOtpReq, ResetPasswordReq } from 'src/app/types/interface.model';
import { ConfirmedValidator } from '../../../validators/confirm-password.validator';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  hide: boolean = true;
  passwordHide: boolean = true;
  confirmPasswordHide: boolean = true;
  userEmailOrPhone: string;
  resetPasswordForm = new FormGroup({});

  constructor(
    private _preAuthService: PreAuthService,
    public formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this._preAuthService.userEmailOrPhone.subscribe(emailOrPhone => {
      this.userEmailOrPhone = emailOrPhone;
    })
    this.createForm();
  }

  createForm() {
    this.resetPasswordForm = this.formBuilder.group({
      'newPassword': ['', [Validators.required, Validators.minLength(6)]],
      'confirmPassword': ['', [Validators.required]],
      'otp': [null, [Validators.required]],
    }, {
      validator: ConfirmedValidator('newPassword', 'confirmPassword')
    })
  }

  onSubmit(val) {
    if (!this.userEmailOrPhone || this.userEmailOrPhone == null || this.userEmailOrPhone == '') {
      this.toastr.error("Please login or signup again", "No user email or phone number has been selected");
      return false;
    }
    if (!this.resetPasswordForm.valid) {
      this.toastr.error("Invalid input", "Please try again");
      return false;
    }

    const userDetails: ResetPasswordReq = {
      emailOrPhone: this.userEmailOrPhone,
      newPassword: val.newPassword,
      otp: val.otp
    }

    this._preAuthService.resetPassword(userDetails).subscribe((res) => {
      this.toastr.success("Your password has been successfully changed", "Success!");
      this.router.navigate(['/login']);
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

  resendOtp() {
    if (!this.userEmailOrPhone || this.userEmailOrPhone == null || this.userEmailOrPhone == '') {
      this.toastr.error("Please login or signup again", "No user email has been selected");
      return false;
    }

    const userDetails: ResendOtpReq = { emailOrPhone: this.userEmailOrPhone };

    this._preAuthService.resendOtp(userDetails).subscribe((res) => {
      this.toastr.success("Please Check your email", "New OTP sent");
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

}
