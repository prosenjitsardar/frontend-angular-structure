import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PreAuthRoutingModule } from './pre-auth-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { PreAuthComponent } from './pre-auth.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { LoginWithOtpComponent } from './login-with-otp/login-with-otp.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PreAuthService } from '../../services/pre-auth.service';
import { CryptoService } from './../../services/utilities/crypto.service';
import { LocalStorageService } from './../../services/utilities/local-storage.service';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [PreAuthComponent, LoginComponent, SignupComponent, LoginWithOtpComponent, ForgotPasswordComponent, ResetPasswordComponent],
  imports: [
    CommonModule,
    PreAuthRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    ReactiveFormsModule,
  ],
  providers: [PreAuthService, CryptoService, LocalStorageService]
})
export class PreAuthModule { }
