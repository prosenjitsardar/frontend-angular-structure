import { PreAuthService } from '../../../services/pre-auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LoginWithOtpReq, ResendOtpReq } from '../../../types/interface.model';

@Component({
  selector: 'app-login-with-otp',
  templateUrl: './login-with-otp.component.html',
  styleUrls: ['./login-with-otp.component.scss']
})
export class LoginWithOtpComponent implements OnInit {

  userEmail: string;
  hide: boolean = true;
  otpVerifyForm: FormGroup;

  constructor(
    private _preAuthService: PreAuthService,
    public formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this._preAuthService.userEmail.subscribe(email => {
      this.userEmail = email;
    })
    this.createForm();
  }

  createForm() {
    this.otpVerifyForm = this.formBuilder.group({
      'otp': ['', [Validators.required]]
    })
  }

  onSubmit(val) {
    if (!this.userEmail || this.userEmail == null || this.userEmail == '') {
      this.toastr.error("Please login or signup again", "No user email has been selected");
      return false;
    }
    if (!this.otpVerifyForm.valid) {
      this.toastr.warning("Invalid input", "Please try again");
      return false;
    }

    const userDetails: LoginWithOtpReq = {
      email: this.userEmail,
      otp: val.otp
    }

    this._preAuthService.loginWithOtp(userDetails).subscribe((res) => {
      this._preAuthService.storeAuthInfo(res);
      this.router.navigate(['/home']);
    }, (err) => {
      this.toastr.error(err.message);
    })

  }

  resendOtp() {
    if (!this.userEmail || this.userEmail == null || this.userEmail == '') {
      this.toastr.error("Please login or signup again", "No user email has been selected");
      return false;
    }

    const userDetails: ResendOtpReq = { emailOrPhone: this.userEmail };

    this._preAuthService.resendOtp(userDetails).subscribe((res) => {
      this.toastr.success("Please Check your email", "New OTP sent");
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

}
