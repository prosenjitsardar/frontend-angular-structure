import { PreAuthService } from '../../../services/pre-auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SignupReq } from '../../../types/interface.model'
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  hide: boolean = true;
  signupForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private _preAuthServcie: PreAuthService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.signupForm = this.formBuilder.group({
      'firstName': ['', [Validators.required, Validators.minLength(3)]],
      'lastName': ['', [Validators.required, Validators.minLength(3)]],
      'email': ['', [Validators.required, Validators.email, Validators.minLength(3)]],
      'phone': ['', [Validators.required, Validators.pattern('[0-9]{10}$')]],
      'password': ['', [Validators.required, Validators.minLength(6)]],
    })
  }

  onSubmit(val) {
    if (!this.signupForm.valid) {
      this.toastr.warning("Invalid data input. Please try again");
      return false;
    }

    const userDetails: SignupReq = val;

    this._preAuthServcie.signup(userDetails).subscribe((res) => {
      this.toastr.success("Please Check your email", "Congratulations");
      this._preAuthServcie.userEmail.next(userDetails.email);
      this.router.navigate(['/login-with-otp']);

    }, (err) => {
      this.toastr.error(err.message);
    })
  }

}
