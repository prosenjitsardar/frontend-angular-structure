import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PreAuthService } from 'src/app/services/pre-auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ForgotPasswordReq } from '../../../types/interface.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;

  constructor(
    private _preAuthService: PreAuthService,
    public formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.forgotPasswordForm = this.formBuilder.group({
      'emailOrPhone': [null, [Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)]],
    })
  }

  onSubmit(val) {
    if (!this.forgotPasswordForm.valid) {
      this.toastr.warning("Invalid input", "Please try again");
      return false;
    }

    let userDetails: ForgotPasswordReq = val;

    this._preAuthService.forgotPassword(userDetails).subscribe((res) => {
      this.toastr.success("Please check your email", "New OTP sent");
      this._preAuthService.userEmailOrPhone.next(userDetails.emailOrPhone);
      this.router.navigate(['/reset-password']);
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

}
