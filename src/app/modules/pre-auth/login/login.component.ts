import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreAuthService } from 'src/app/services/pre-auth.service';
import { LoginReq } from 'src/app/types/interface.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide: boolean = true;
  loginForm: FormGroup;

  constructor(
    private _preAuthService: PreAuthService,
    public formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      'emailOrPhone': [null, [Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)]],
      'password': ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  onSubmit(val) {
    if (!this.loginForm.valid) {
      this.toastr.warning("Invalid input", "Please try again");
      return false;
    }

    let userDetails: LoginReq = val;

    this._preAuthService.login(userDetails).subscribe((res) => {
      this._preAuthService.storeAuthInfo(res);
      this.router.navigate(['/home']);
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

}
