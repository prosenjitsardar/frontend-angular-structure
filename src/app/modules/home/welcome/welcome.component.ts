import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { PreAuthService } from 'src/app/services/pre-auth.service';
import { ToastrService } from 'ngx-toastr';
import { IUser } from 'src/app/types/interface.model';
import { Config } from 'src/app/config';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  private readonly demoProfileImage: string = "../../../../assets/no-profile-image.png";
  userProfileImage: string = this.demoProfileImage;
  userName: string;
  previewImage: boolean = false;
  image: any = null;

  constructor(
    private _userService: UserService,
    private _preAuthService: PreAuthService,
    private Router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getUserDetails();
  }

  private getUserDetails() {
    this._userService.getUserDetails().subscribe((res) => {
      const userDetails: IUser = res;
      this._userService.userDetails.next(userDetails);
      this.setUserName(userDetails.name.first);
      this.setUserProfileImage(userDetails.profileImage);
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

  private setUserName(userName: string) {
    this.userName = userName;
  }

  private setUserProfileImage(userImage: any) {
    if (userImage != null) {
      this.userProfileImage = `${Config.SERVER_HOST}${userImage}`;
    }
  }

  closeImagePreview() {
    this.previewImage = false;
  }

  sendImgToImagePreview(fileInputEvent: any) {
    if (!fileInputEvent.target.files[0]) {
      this.toastr.error("Plese choose an image");
      return false;
    }
    this.image = fileInputEvent;
    this.previewImage = true;
  }

  uploadImg(file: File) {
    this.previewImage = false;
    const image = file;
    const formData = new FormData()
    formData.append('type', 'profileImage');
    formData.append('images', image);
    this._userService.uploadProfileImage(formData).subscribe((res) => {
      this.toastr.success("Your profile picture has been uploaded successfully", "Success!");
      this.getUserDetails();
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

  logout() {
    this._preAuthService.logout();
  }

}
