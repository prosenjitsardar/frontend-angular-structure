import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ImageCroppedEvent, base64ToFile } from 'ngx-image-cropper';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss']
})
export class ImagePreviewComponent implements OnInit {

  @Input() image: any;
  @Output() close = new EventEmitter<void>();
  @Output() finalImage = new EventEmitter<File>();
  imageChangedEvent: any;
  croppedImage: any = '';
  imageFile: File;

  constructor(
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.setImageChangeEvent();
  }

  setImageChangeEvent() {
    this.imageChangedEvent = this.image;
  }

  loadImageFailed() {
    this.toastr.error("Please upload the image again", "Something went wrong!");
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;

    this.imageFile = this.base64ToFile(
      event.base64,
      this.imageChangedEvent.target.files[0].name,
    )
  }

  base64ToFile(data, filename) {
    const arr = data.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    let u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }

  onUpload() {
    const image = this.imageFile;
    this.finalImage.emit(image);
  }

  onClose() {
    this.close.emit();
  }


  // someFunction(): File {
  //   // Assuming you have stored the event.base64 in an instance variable 'croppedImage'
  //   const file: File = new File([base64ToFile(this.croppedImage)], 'fileName.png');
  //   return file;
  // }

}
