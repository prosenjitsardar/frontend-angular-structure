import { ChangePasswordReq } from './../../../types/interface.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PreAuthService } from 'src/app/services/pre-auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ConfirmedValidator } from 'src/app/validators/confirm-password.validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  hide: boolean = true;
  passwordHide: boolean = true;
  confirmPasswordHide: boolean = true;
  changePasswordForm: FormGroup;

  constructor(
    private _preAuthService: PreAuthService,
    public formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.changePasswordForm = this.formBuilder.group({
      'password': ['', [Validators.required, Validators.minLength(6)]],
      'newPassword': ['', [Validators.required, Validators.minLength(6)]],
      'confirmPassword': ['', [Validators.required]],
    }, {
      validator: ConfirmedValidator('newPassword', 'confirmPassword')
    })
  }

  onSubmit(val) {
    if (!this.changePasswordForm.valid) {
      this.toastr.error("Invalid input", "Please try again");
      return false;
    }

    const userDetails: ChangePasswordReq = {
      password: val.password,
      newPassword: val.newPassword
    }

    this._preAuthService.changePassword(userDetails).subscribe((res) => {
      this.toastr.success("Your password has been changed", "Success!");
      this.router.navigate(['/home']);
    }, (err) => {
      this.toastr.error(err.message);
    })
  }

}
