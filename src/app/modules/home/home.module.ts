import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { WelcomeComponent } from './welcome/welcome.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { PreAuthService } from 'src/app/services/pre-auth.service';
import { LocalStorageService } from 'src/app/services/utilities/local-storage.service';
import { UserService } from './../../services/user.service';
import { ImagePreviewComponent } from './image-preview/image-preview.component';
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  declarations: [HomeComponent, ChangePasswordComponent, WelcomeComponent, ImagePreviewComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    ReactiveFormsModule,
    ImageCropperModule
  ],
  providers: [
    PreAuthService,
    LocalStorageService,
    UserService
  ]
})
export class HomeModule { }
