import { PreAuthService } from 'src/app/services/pre-auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate, CanLoad {

  constructor(private _preAuthService: PreAuthService, private router: Router) { }

  canActivate() {
    return this.canLoad();
  }
  canLoad() {
    if (!this._preAuthService.isLoggedIn()) {
      this.router.navigate(['/login']);
    }
    return this._preAuthService.isLoggedIn();
  }
}
